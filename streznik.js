var express = require('express'), path = require('path'), fs = require('fs');

var app = express();
app.use(express.static(__dirname + '/public'));

var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Zunanja avtentikacija)
 */
app.get('/api/prijava', function(req, res) {
	var uporabniskoIme = req.query.uporabniskoIme;
	var geslo = req.query.geslo;
	var stat, err;
	if((uporabniskoIme == null|| uporabniskoIme.length == 0) || (geslo == null && geslo.length == 0)) {
		stat = false; 
		err = "Napačna zahteva.";
	}
	else if(preveriSpomin(uporabniskoIme, geslo) || preveriDatotekaStreznik(uporabniskoIme, geslo)) {
		stat = true;
		err = "";
		
		
	} else {
		stat = false;
		err = "Avtentikacija ni uspela."
	}
	res.send({status: stat, napaka: err});
	// ...
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Prijava uporabnika v sistem)
 */
app.get('/prijava', function(req, res) {
	var vrni;
	var uporabniskoIme = req.query.uporabniskoIme;
	var geslo = req.query.geslo;
	if(preveriSpomin(uporabniskoIme, geslo) || preveriDatotekaStreznik(uporabniskoIme, geslo)) {
		vrni = "<html><title>Uspešno</title><body><p>Uporabnik <b>"+uporabniskoIme+"</b> uspešno prijavljen v sistem!</p></body></html>";
	} else {
		vrni = "<html><title>Napaka</title><body><p>Uporabnik <b>"+uporabniskoIme+"</b> nima pravice prijave v sistem!</p></body></html>";
	}
	
	res.send(vrni);
	// ...
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (branje datoteka na strani strežnika)
 */
 var podatkiDatotekaStreznik = JSON.parse(fs.readFileSync(__dirname + "/public/podatki/" + "uporabniki_streznik.json").toString());



/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
function preveriSpomin(uporabniskoIme, geslo) {
	var vrni = false;
				var uI, g;
				for(var i in podatkiSpomin) {
					uI = podatkiSpomin[i].split("/")[0];
					g =  podatkiSpomin[i].split("/")[1];
					if(uporabniskoIme == uI && geslo == g) {
						vrni = true; break;
					}
				}
				return vrni;
}


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
function preveriDatotekaStreznik(uporabniskoIme, geslo) {
	var uI, g, vrni = false;
				for(var i in podatkiDatotekaStreznik) {
					uI = podatkiDatotekaStreznik[i]["uporabnik"];
					g = podatkiDatotekaStreznik[i]["geslo"];
					if(uI == uporabniskoIme && g == geslo) {
						vrni = true; break;
					}
				}
				return vrni;
}